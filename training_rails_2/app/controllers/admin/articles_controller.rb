class Admin::ArticlesController < Admin::ApplicationController
  before_filter :require_admin_login, :only => [:new, :create, :edit, :update, :destroy]
  def index
    @articles = Article.all
  end

  def show
    @articles = Article.find(params[:id])
    @comments = Comment.find_all_by_article_id(params[:id])
    @comment  = Comment.new
  end
  
  def new
    @articles_add = Article.new
  end
  
  def create
    @articles_add = Article.new(params[:article])
    if @articles_add.save
        redirect_to :action => 'index'
    else
        render :action => 'new'
    end
  end
  
  def edit
    @article = Article.find(params[:id])
  end
  
  def update
          @article = Article.find(params[:id])
          if @article.update_attributes(params[:article])
             redirect_to :action => 'show', :id => @article
          else
             render :action => 'edit'
          end
  end
  
  def destroy
    Article.find(params[:id]).destroy
    redirect_to :action => 'index'
  end
end
