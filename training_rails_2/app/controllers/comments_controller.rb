class CommentsController < ApplicationController
  before_filter :require_login, :only => [:destroy]
  def index
    @comments = Comment.all
  end
  
  def show
    @comments = Comment.find(params[:id])
  end
  
  def new
    @comments = Comment.new
  end
  
  def create
    @comments = Comment.new(params[:comment])
    respond_to do |format|
      if @comments.save
        flash[:notice] = "Success Entry to Comments"
        format.html { redirect_to(article_path(article), :notice => 'Comment was successfully created.') }
        format.js { @comments = Article.find(params[:comment][:article_id].to_i).comments }
      end
    end
  end
  
  def destroy
    Comment.find(params[:id]).destroy
    redirect_to :action => 'index'
  end
end
