class ProductsController < ApplicationController
  before_filter :require_login, :only => [:new, :create, :edit, :update, :destroy]
  def index
    flash[:notice] = "Success Entry to Products"
    @products = Product.all
  end

  def show
    @products = Product.find(params[:id])
  end
  
  def new
    @products_add = Product.new
  end
  
  def create
    @products_add = Product.new(params[:product])
    if @products_add.save
        redirect_to :action => 'index'
    else
        render :action => 'new'
    end
  end
  
  def edit
    @product = Product.find(params[:id])
  end
  
  def update
          @product = Product.find(params[:id])
          if @product.update_attributes(params[:product])
             redirect_to :action => 'show', :id => @product
          else
             render :action => 'edit'
          end
  end
  
  def destroy
    Product.find(params[:id]).destroy
    redirect_to :action => 'index'
  end
end
