class UsersController < ApplicationController
  before_filter :require_login, :only => [:new, :create, :edit, :update, :destroy]
  def index
    @users = User.all
  end

  def show
    @users = User.find(params[:id])
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(params[:user])
    if verify_recaptcha
      if @user.save
          flash[:notice] = "Success Entry to Users"
          UserMailer.registration_confirmation(@user).deliver
          redirect_to root_url, :notice =>"Signed Up!"
      else
          flash[:error] = "Failed Entry (Something Wrong)"
          render 'new'
      end
    else
      #flash[:error] = "There was an error with the recaptcha code below.
      #                Please re-enter the code and click submit."
      render "new"
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
          @user = User.find(params[:id])
          if @user.update_attributes(params[:user])
             redirect_to :action => 'show', :id => @user
          else
             render :action => 'edit'
          end
  end
  
  def destroy
    User.find(params[:id]).destroy
    redirect_to :action => 'index'
  end
end
