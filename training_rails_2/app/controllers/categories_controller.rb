class CategoriesController < ApplicationController
  before_filter :require_login, :only => [:new, :create, :edit, :update, :destroy]
  def index
    @categories = Category.all
  end
  
  def show
    @categories = Category.find(params[:id])
  end
  
  def new
    @categories_add = Category.new
  end
  
  def create
    @categories_add = Category.new(params[:category])
    if @categories_add.save
        flash[:notice] = "Success Entry to Categories"
        redirect_to :action => 'index'
    else
        flash[:error] = "Failed Entry (Something Wrong)"
        render :action => 'new'
    end
  end
  
  def edit
    @category = Category.find(params[:id])
  end
  
  def update
          @category = Category.find(params[:id])
          if @category.update_attributes(params[:category])
             redirect_to :action => 'show', :id => @category
          else
             render :action => 'edit'
          end
  end
  
  def destroy
    Category.find(params[:id]).destroy
    redirect_to :action => 'index'
  end
end
