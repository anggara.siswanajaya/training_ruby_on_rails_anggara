class ArticlesController < ApplicationController
  before_filter :require_login, :only => [:new, :create, :edit, :update, :destroy]
  def index
    @articles = Article.find_all_by_user_id(session[:user_id])
  end

  def show
    @articles = Article.find(params[:id])
    @comments = Comment.find_all_by_article_id(params[:id])
    @comment  = Comment.new
  end
  
  def new
    @article = Article.new
  end
  
  def create
    @article = Article.new(params[:article])
    if @article.save
        flash[:notice] = "Success Entry to Article"
        redirect_to :action => 'index'
    else
        flash[:error] = "Failed Entry (Something Wrong)"
        render :action => 'new'
    end
  end
  
  def edit
    @article = Article.find(params[:id])
  end
  
  def update
          @article = Article.find(params[:id])
          if @article.update_attributes(params[:article])
             redirect_to :action => 'show', :id => @article
          else
             render :action => 'edit'
          end
  end
  
  def destroy
    Article.find(params[:id]).destroy
    redirect_to :action => 'index'
  end
end
