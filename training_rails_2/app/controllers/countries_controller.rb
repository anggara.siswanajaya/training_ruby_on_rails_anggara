class CountriesController < ApplicationController
  before_filter :require_login, :only => [:new, :create, :edit, :update, :destroy]
  def index
    @countries = Country.all
  end

  def show
    @countries = Country.find(params[:id])
  end
  
  def new
    @countries_add = Country.new
  end
  
  def create
    @countries_add = Country.new(params[:country])
    if @countries_add.save
        flash[:notice] = "Success Entry to Countries"
        redirect_to :action => 'index'
    else
        flash[:error] = "Failed Entry (Something Wrong)"
        render :action => 'new'
    end
  end
  
  def edit
    @country = Country.find(params[:id])
  end
  
  def update
          @country = Country.find(params[:id])
          if @country.update_attributes(params[:country])
             redirect_to :action => 'show', :id => @country
          else
             render :action => 'edit'
          end
  end
  
  def destroy
    Country.find(params[:id]).destroy
    redirect_to :action => 'index'
  end
end
