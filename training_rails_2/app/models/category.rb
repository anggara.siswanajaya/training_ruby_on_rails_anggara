class Category < ActiveRecord::Base
  attr_accessible :name, :user_id
  belongs_to :user
  has_many :products, :through => :categories_products
  has_many :categories_products
  scope :search, -> (search) {where name: search}
  has_and_belongs_to_many :search_products,
                           :class_name => "Product" ,
                           :foreign_key => "category_id",
                           :conditions => "name like '%shoes%'"
                           
  validates :name,    :presence => {:message => "You must filled name"}
  
  validates :user_id,    :presence => {:message => "You must filled user_id"}
end
