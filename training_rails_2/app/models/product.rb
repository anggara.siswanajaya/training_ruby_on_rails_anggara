class Product < ActiveRecord::Base
  attr_accessible :name, :price, :stock, :description, :user_id
  belongs_to :user
  has_many :categories, :through => :categories_products
  has_many :categories_products
  scope :stock, -> (stock) {where stock: stock}
  scope :under1k, -> {where("price < '1000'")}
  
  validates :price,   :presence => {:message => "You must filled username"},
                      :numericality => true
              
  validates :name,   :presence => {:message => "You must filled name"}
  
  validates :stock,   :presence => {:message => "You must filled stock"}
  
  validates :description,   :presence => {:message => "You must filled description"}
  
  validates :user_id,   :presence => {:message => "You must filled user_id"}
end