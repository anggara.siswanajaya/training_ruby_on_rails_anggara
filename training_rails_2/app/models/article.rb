class Article < ActiveRecord::Base
  attr_accessible :title, :description, :rating, :user_id
  belongs_to :user
  has_many :comments, :dependent => :destroy
  scope :rating, where("rating > ?", 0)
  
  validates :title,   :presence => {:message => "You must filled title"},
                      :uniqueness => true,
                      :format => {:with => /[a-zA-Z\s]+$/}
  
  validates :description,   :presence => {:message => "You must filled title"}
  
  validates :rating,   :presence => {:message => "You must filled rating"}
  
  validates :user_id,   :presence => {:message => "You must filled user_id"}
                      
  validate :valid_title

  def valid_title
    self.errors[:title] << "Can't filled by 'nil, empty, and blank'" if (title == 'nil' || title == 'empty' || title == 'blank')
  end
                      
  def self.show_length
    where("length(description) > 100")
  end
end
