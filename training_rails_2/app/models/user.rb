class User < ActiveRecord::Base
  attr_accessible :first_name, :last_name, :email, :username, :password, :password_confirmation, :date_of_birth, :age, :address, :country_id
  attr_accessor :password
  has_many :products
  has_many :articles
  has_many :categories
  has_many :comments
  scope :indonesia, -> {where( "country_id = '1'")}
  belongs_to :country
  has_many :search_title,
             :class_name => "Article",
               :foreign_key => "user_id",
               :conditions => "title like '%my country%'"
  validates :first_name,  :presence => {:message => "You must filled first name"}, 
                          :uniqueness => true,
                          :length => {:minimum => 1, :maximum => 25},
                          :format => {:with => /[a-zA-Z\s]+$/}
                          
  validates :last_name,   :presence => {:message => "You must filled last name"}, 
                          :uniqueness => true,
                          :length => {:minimum => 1, :maximum => 25},
                          :format => {:with => /[a-zA-Z\s]+$/}
                          
  validates :email,       :presence => {:message => "You must filled email"},
                          :uniqueness => true,
                          :length => {:minimum => 3, :maximum => 100},
                          :format => {:with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i}
                          
  validates :username,    :presence => {:message => "You must filled username"}
  
  validates :password,    :presence => {:on => :create},
                          :confirmation => true
                          
  validates :date_of_birth,  :presence => {:message => "date_of_birth"}
  
  validates :age,  :presence => {:message => "You must filled age"}
  
  validates :address,  :presence => {:message => "You must filled address"}
  
  validates :country_id,  :presence => {:message => "You must filled country id"}
  
  before_save :encrypt_password
  def encrypt_password
    if password.present?
            self.password_salt = BCrypt::Engine.generate_salt
            self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
       end
  end
  
  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
  
  def show_address_country
    country = Country.find(self.country_id)
    "#{self.address} #{country.name}"
  end
  
  def self.show_age
    where("age > '18'")
  end
end
