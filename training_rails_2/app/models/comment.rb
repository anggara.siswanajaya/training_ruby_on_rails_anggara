class Comment < ActiveRecord::Base
  attr_accessible :content, :user_id, :article_id
  belongs_to :user
  belongs_to :article
  
  validates :content,   :presence => {:message => "You must filled content"}
  
  validates :user_id,   :presence => {:message => "You must filled title"}
  
  validates :article_id,   :presence => {:message => "You must filled title"}
end
