class Country < ActiveRecord::Base
  attr_accessible :code, :name
  has_many :users
  has_many :search_user_indo,
             :class_name => "User" ,
               :foreign_key => "country_id",
               :conditions => "country_id = '1'"
               
  validates :code,    :presence => {:message => "You must filled code"}
  
  validates :name,    :presence => {:message => "You must filled name"}
               
  validate :valid_code

  def valid_code
    self.errors[:code] << "Only can filled by 'id, usa, and chn'" unless code == 'id' || code == 'usa' || code == 'chn'
  end
end
