require 'test_helper' 

class UserTest < ActiveSupport::TestCase 

  def test_save_without_first_name
    user = User.new(:last_name => 'last_name_test', :email => 'email@email.com', :username => 'usertest',
                    :password => 'usertest', :date_of_birth => '31-08-1991', :age => '22',
                    :address => 'Arcamanik Bandung', :country_id => '1' )
    assert_equal user.valid?, false
    assert_equal user.save, false
  end
  
  def test_save_without_last_name
    user = User.new(:first_name => 'last_name_test', :email => 'email@email.com', :username => 'usertest',
                    :password => 'usertest', :date_of_birth => '31-08-1991', :age => '22',
                    :address => 'Arcamanik Bandung', :country_id => '1' )
    assert_equal user.valid?, false
    assert_equal user.save, false
  end
  
  def test_save_without_email
    user = User.new(:first_name => 'first_name_test', :last_name => 'last_name_test', :username => 'usertest',
                    :password => 'usertest', :date_of_birth => '31-08-1991', :age => '22',
                    :address => 'Arcamanik Bandung', :country_id => '1' )
    assert_equal user.valid?, false
    assert_equal user.save, false
  end
  
  def test_save_without_username
    user = User.new(:first_name => 'first_name_test', :last_name => 'last_name_test', :email=> 'email@email.com',
                    :password => 'usertest', :date_of_birth => '31-08-1991', :age => '22',
                    :address => 'Arcamanik Bandung', :country_id => '1' )
    assert_equal user.valid?, false
    assert_equal user.save, false
  end
  
  def test_save_without_password
    user = User.new(:first_name => 'first_name_test', :last_name => 'last_name_test', :email=> 'email@email.com',
                    :username => 'usertest', :date_of_birth => '31-08-1991', :age => '22',
                    :address => 'Arcamanik Bandung', :country_id => '1' )
    assert_equal user.valid?, false
    assert_equal user.save, false
  end
  
  def test_save_without_date_of_birth
    user = User.new(:first_name => 'first_name_test', :last_name => 'last_name_test', :email=> 'email@email.com',
                    :username => 'usertest', :password => 'password', :age => '22',
                    :address => 'Arcamanik Bandung', :country_id => '1' )
    assert_equal user.valid?, false
    assert_equal user.save, false
  end
  
  def test_save_without_age
    user = User.new(:first_name => 'first_name_test', :last_name => 'last_name_test', :email=> 'email@email.com',
                    :username => 'usertest', :password => 'password', :date_of_birth => '31-08-1991',
                    :address => 'Arcamanik Bandung', :country_id => '1' )
    assert_equal user.valid?, false
    assert_equal user.save, false
  end
  
  def test_save_without_address
    user = User.new(:first_name => 'first_name_test', :last_name => 'last_name_test', :email=> 'email@email.com',
                    :username => 'usertest', :password => 'password', :date_of_birth => '31-08-1991',
                    :age => '22', :country_id => '1' )
    assert_equal user.valid?, false
    assert_equal user.save, false
  end
  
  def test_save_without_country_id
    user = User.new(:first_name => 'first_name_test', :last_name => 'last_name_test', :email=> 'email@email.com',
                    :username => 'usertest', :password => 'password', :date_of_birth => '31-08-1991',
                    :age => '22', :address => 'Arcamanik Bandung' )
    assert_equal user.valid?, false
    assert_equal user.save, false
  end
   
  def test_relation_between_user_and_article
    user = User.create(:first_name => 'first_name_test', :last_name => 'last_name_test', :email=> 'email@email.com',
                       :username => 'usertest', :password => 'password', :password_confirmation => 'password', :date_of_birth => '31-08-1991',
                       :age => '22', :address => 'Arcamanik Bandung', :country_id => 1 )
    assert_not_nil user
    article = Article.create(:user_id => user.id, :title => "new_title", :description => "new content",:rating => "3")
    assert_not_nil user.articles
    assert_equal user.articles.empty?, false
    assert_equal user.articles[0].class, Article
  end
  
  def test_relation_between_user_and_comment
    user = User.create(:first_name => 'first_name_test', :last_name => 'last_name_test', :email=> 'email@email.com',
                       :username => 'usertest', :password => 'password', :date_of_birth => '31-08-1991',
                       :age => '22', :address => 'Arcamanik Bandung', :country_id => 1 )
    assert_not_nil user
    comment = Comment.create(:user_id => user.id, :content => "my comment",  :article_id => "3")
    assert_not_nil user.comments
    assert_equal user.comments.empty?, false
    assert_equal user.comments[0].class, Comment
  end
  
  def test_relation_between_user_and_product
    user = User.create(:first_name => 'first_name_test', :last_name => 'last_name_test', :email=> 'email@email.com',
                       :username => 'usertest', :password => 'password', :date_of_birth => '31-08-1991',
                       :age => '22', :address => 'Arcamanik Bandung', :country_id => 1 )
    assert_not_nil user
    product = Product.create(:user_id => user.id, :name => "your_name", :price => 20000, :stock => 5, :description => 'your_description')
    assert_not_nil user.products
    assert_equal user.products.empty?, false
    assert_equal user.products[0].class, Product
  end
  
  def test_relation_between_user_and_category
    user = User.create(:first_name => 'first_name_test', :last_name => 'last_name_test', :email=> 'email@email.com',
                       :username => 'usertest', :password => 'password', :date_of_birth => '31-08-1991',
                       :age => '22', :address => 'Arcamanik Bandung', :country_id => 1 )
    assert_not_nil user
    category = Category.create(:user_id => user.id, :name => "category_name")
    assert_not_nil user.categories
    assert_equal user.categories.empty?, false
    assert_equal user.categories[0].class, Category
  end

end