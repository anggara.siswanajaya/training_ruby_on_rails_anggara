require 'test_helper' 

class CategoryTest < ActiveSupport::TestCase 

  def test_save_without_name
    category = Category.new(:user_id => '1' )
    assert_equal category.valid?, false
    assert_equal category.save, false
  end
  
  def test_save_without_user_id
    category = Category.new(:name => 'test' )
    assert_equal category.valid?, false
    assert_equal category.save, false
  end

  def test_relation_between_category_and_product
      category = Category.create(:name=>'new_cat',:user_id=>'2')
      assert_not_nil category
      product = Product.create(:name => 'new_prod', :price => "20000",:stock=>32,:description=>"new_desc",:user_id=>"2")
      assert_not_nil product
      cat_prod = CategoriesProduct.create(:category_id=>category.id, :product_id=>product.id)
      assert_not_nil category.products
      assert_equal category.products.empty?, false
      assert_equal category.products[0].class, Product
  end

end