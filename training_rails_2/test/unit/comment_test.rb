require 'test_helper' 

class CommentTest < ActiveSupport::TestCase 

  def test_save_without_content
    comment = Comment.new(:article_id => "3", :user_id => '1' )
    assert_equal comment.valid?, false
    assert_equal comment.save, false
  end
  
  def test_save_without_article_id
    comment = Comment.new(:content => "my comment", :user_id => '1' )
    assert_equal comment.valid?, false
    assert_equal comment.save, false
  end
  
  def test_save_without_user_id
    comment = Comment.new(:content => "my comment",  :article_id => "3")
    assert_equal comment.valid?, false
    assert_equal comment.save, false
  end

end