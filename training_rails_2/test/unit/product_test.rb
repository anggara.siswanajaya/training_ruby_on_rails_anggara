require 'test_helper' 

class ProductTest < ActiveSupport::TestCase 

  def test_save_without_name
    product = Product.new(:price => 20000, :stock => 5, :description => 'item_description', :user_id => '1' )
    assert_equal product.valid?, false
    assert_equal product.save, false
  end
  
  def test_save_without_price
    product = Product.new(:name => 'item_name', :stock => 5, :description => 'item_description', :user_id => '1' )
    assert_equal product.valid?, false
    assert_equal product.save, false
  end
  
  def test_save_without_stock
    product = Product.new(:name => 'item_name', :price => 20000, :description => 'item_description', :user_id => '1' )
    assert_equal product.valid?, false
    assert_equal product.save, false
  end
  
  def test_save_without_description
    product = Product.new(:name => 'item_name', :price => 20000, :stock => 5, :user_id => '1' )
    assert_equal product.valid?, false
    assert_equal product.save, false
  end
  
  def test_save_without_user_id
    product = Product.new(:name => 'item_name', :price => 20000, :stock => 5, :description => 'blablabla' )
    assert_equal product.valid?, false
    assert_equal product.save, false
  end

  def test_relation_between_product_and_category
      product = Product.create(:name => 'new_prod', :price => "20000",:stock=>32,:description=>"new_desc",:user_id=>"2")
      assert_not_nil product
      category = Category.create(:name=>'new_cat',:user_id=>'2')
      assert_not_nil category
      cat_prod = CategoriesProduct.create(:product_id=>product.id, :category_id=>category.id)
      assert_not_nil product.categories
      assert_equal product.categories.empty?, false
      assert_equal product.categories[0].class, Category
  end

end