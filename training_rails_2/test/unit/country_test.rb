require 'test_helper' 

class CountryTest < ActiveSupport::TestCase 

  def test_save_without_code
    country = Country.new(:name => 'name_test' )
    assert_equal country.valid?, false
    assert_equal country.save, false
  end
  
  def test_save_without_name
    country = Country.new(:code => '1' )
    assert_equal country.valid?, false
    assert_equal country.save, false
  end

  # def test_relation_between_country_and_user
  #     country = Country.create(:code => '1', :name => 'name_test')
  #     assert_not_nil country
  #     user = User.create(:country_id => country.id, :first_name => 'first_name_test', :last_name => 'last_name_test', :email=> 'email@email.com',
  #                        :username => 'usertest', :password => 'password', :date_of_birth => '31-08-1991',
  #                        :age => '22', :address => 'Arcamanik Bandung' )
  #     assert_not_nil country.users
  #     assert_equal country.users.empty?, false
  #     assert_equal country.users[0].class, User
  # end

end