require 'test_helper' 

class ArticleTest < ActiveSupport::TestCase 

  def test_save_without_title
    article = Article.new(:description => 'new_description', :rating => '1', :user_id => '1' )
    assert_equal article.valid?, false
    assert_equal article.save, false
  end
  
  def test_save_without_description
    article = Article.new(:title => 'tester', :rating => '1', :user_id => '1' )
    assert_equal article.valid?, false
    assert_equal article.save, false
  end
  
  def test_save_without_rating
    article = Article.new(:title => 'tester', :description => 'new_desc', :user_id => '1' )
    assert_equal article.valid?, false
    assert_equal article.save, false
  end
  
  def test_save_without_user_id
    article = Article.new(:title => 'tester', :description => 'new_desc', :rating => '5' )
    assert_equal article.valid?, false
    assert_equal article.save, false
  end
  
  def test_find_top_article
    Article.create([
     {:title => "title xxx", :description => "test_desc_xxx", :rating => '5', :user_id => '1'},
     {:title => "title zzz", :description => "test_desc_zzz", :rating => '4', :user_id => '1'}, 
    ])
    assert_not_nil Article.rating
    assert_equal Article.rating[0].title, "title xxx"
  end
  
  def test_relation_between_article_and_comment
     article = Article.create(:title => "new_title", :description => "new content",:rating=>"3",:user_id=>"3")
     assert_not_nil article
     comment = Comment.create(:article_id => article.id, :content => "my comment",  :user_id => "22")
     assert_not_nil article.comments
     assert_equal article.comments.empty?, false
     assert_equal article.comments[0].class, Comment
  end

end