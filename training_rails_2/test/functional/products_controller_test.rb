require 'test_helper'

class ProductsControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  def setup
    @product = Product.find(:first)
  end
  
  def test_index
    login_as('angga_emplud@yahoo.com')
    get :index
    assert_response :success
    assert_not_nil assigns(:products)
  end
end
