require 'test_helper'

class ArticlesControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  def setup
    @article = Article.find(:first)
  end
  
  def test_index
    login_as('angga_emplud@yahoo.com')
    get :index
    assert_response :success
    assert_not_nil assigns(:articles)
  end
  
  def test_new
    login_as('anggara.siswanajaya@gmail.com')
    get :new
    assert_not_nil(:article)
    #assert_response :success
  end
  
  # def test_create
  #    login_as('anggara.siswanajaya@gmail.com')
  #    assert_difference('Article.count') do
  #      articles :create, :article => {:title => 'new_title', :description => 'new description', :rating => 1, :user_id => 1}
  #      assert_not_nil(:article)
  #      assert_equal assigns(:articles).description, "new_title"
  #       assert_equal assigns(:articles).valid?, true
  #   end
  #   #assert_response :redirect
  #   #assert_redirected_to articles_path(assigns(:article))
  #   #assert_equal flash[:notice], 'Post was successfully created.'
  # end
  
  def test_show
      get :show, :id => Article.first.id
      assert_not_nil(:article)
      assert_response :success
  end
  
  def test_update
      put :update, :id => Article.first.id,
                   :article => {:title => "new_tit", :description => "newdesc", :rating => "1", :user_id => "1"}
      assert_not_nil(:article)
      #assert_equal assigns(:article).title, 'updated title'
      #assert_response :redirect
      #assert_redirected_to post_path(assigns(:article))
      #assert_equal flash[:notice], 'Post was successfully updated.'
  end
  
  # def test_destroy
  #     assert_difference('Article.count', -1) do
  #       delete :destroy, :id => Article.first.id
  #       assert_not_nil assigns(:article)
  #     end
  #     assert_response :redirect
  #     assert_redirected_to articles_path
  #     assert_equal flash[:error], 'Post successfully deleted'
  # end
  
end
