class User < ActiveRecord::Base
  has_many :products
  has_many :articles
  has_many :categories
  has_many :comments
  scope :indonesia, -> {where( "country_id = '1'")}
  belongs_to :country
  has_many :certain_articles,
             :class_name => "Article" ,
               :foreign_key => "user_id",
               :conditions => "title likes '%my country%'"
  
  def show_address_country
    country = Country.find(self.country_id)
    "#{self.address} #{country.name}"
  end
  
  def self.show_age
    where("age > '18'")
  end
end
