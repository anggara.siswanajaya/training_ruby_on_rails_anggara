class Product < ActiveRecord::Base
  belongs_to :user
  has_many :categories, :through => :categories_products
  has_many :categories_products
  scope :stock, -> (stock) {where stock: stock}
  scope :under1k, -> {where("price < '1000'")}
end
