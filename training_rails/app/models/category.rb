class Category < ActiveRecord::Base
  belongs_to :user
  has_many :products, :through => :categories_products
  has_many :categories_products
  scope :search, -> (search) {where name: search}
end
