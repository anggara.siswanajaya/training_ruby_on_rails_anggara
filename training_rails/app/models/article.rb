class Article < ActiveRecord::Base
  belongs_to :user
  has_many :comments, :dependent => :destroy
  scope :rating, -> (rating) {where rating: rating}
  
  def self.show_length
    where("length(description) > 100")
  end
end
