1.		Anggara:training_ruby_on_rails_anggara User$ ruby hello_world.rb
		Hello, World!

2. 1.	2.0.0-p451 :091 > string = "ANGGARA"
 		 => "ANGGARA"
   2.	2.0.0-p451 :092 > string.capitalize
		 => "Anggara"
   3.	2.0.0-p451 :094 > Math.sqrt(16)
 		 => 4.0
   4.	2.0.0-p451 :096 > size = string.size
 		 => 7 

3. 1.	2.0.0-p451 :097 > array = [5,3,2]
 		 => [5, 3, 2] 
		2.0.0-p451 :098 > array.sort
		 => [2, 3, 5]
   2.	2.0.0-p451 :099 > array.max
 		 => 5

4. 1.	2.0.0-p451 :003 > def method1
		2.0.0-p451 :004?>   "Hallo kaka"
		2.0.0-p451 :005?>   end
		 => nil 
		2.0.0-p451 :006 > method1
		=> "Hallo kaka"
		2.0.0-p451 :007 > def method2
		2.0.0-p451 :008?>   "Ini method 2"
		2.0.0-p451 :009?>   end
		 => nil 
		2.0.0-p451 :010 > method2
		 => "Ini method 2" 
		2.0.0-p451 :011 > def method3
		2.0.0-p451 :012?>   "Ini method 3"
		2.0.0-p451 :013?>   end
		 => nil 
		2.0.0-p451 :014 > method3
		 => "Ini method 3" 
		2.0.0-p451 :015 > def method4
		2.0.0-p451 :016?>   "Ini method 4"
		2.0.0-p451 :017?>   end
		 => nil
		2.0.0-p451 :019 > method4
		 => "Ini method 4" 
		2.0.0-p451 :020 > def method5
		2.0.0-p451 :021?>   "Ini method 5"
		2.0.0-p451 :022?>   end
		 => nil 
		2.0.0-p451 :023 > method5
		 => "Ini method 5"
		 
	2.	2.0.0-p451 :026 > def method1(a,b)
		2.0.0-p451 :027?>   total = b - a
		2.0.0-p451 :028?>   return total
		2.0.0-p451 :029?>   end
		 => nil 
		2.0.0-p451 :030 > method1(2,7)
		 => 5 
		2.0.0-p451 :036 > def method2(a,b)
		2.0.0-p451 :037?>   total = a + b
		2.0.0-p451 :038?>   return total
		2.0.0-p451 :039?>   end
		 => nil 
		2.0.0-p451 :040 > method2(3,5)
		 => 8
		2.0.0-p451 :007 > def method3(a,b)
		2.0.0-p451 :008?>   kesatu = a + 5
		2.0.0-p451 :009?>   kedua = b + 2
		2.0.0-p451 :010?>   return kesatu, kedua
		2.0.0-p451 :011?>   end
		 => nil 
		2.0.0-p451 :012 > method3(4,3)
		 => [9, 5]
		2.0.0-p451 :015 > def method4(a,*b)
		2.0.0-p451 :016?>   total = a + b[0] - b[1]
		2.0.0-p451 :017?>   puts "Total Penjumlahan = #{total}"
		2.0.0-p451 :018?>   return total
		2.0.0-p451 :019?>   end
		 => nil 
		2.0.0-p451 :020 > method4(1,3,5)
		Total Penjumlahan = -1
		 => -1
		2.0.0-p451 :021 > def method5(a,*b)
		2.0.0-p451 :022?>   total1 = a + b[0]
		2.0.0-p451 :023?>   total2 = a + b[1]
		2.0.0-p451 :024?>   total_all = total1 + total2
		2.0.0-p451 :025?>   puts "Total Semuanya #{total_all}"
		2.0.0-p451 :026?>   return total1, total2, total_all
		2.0.0-p451 :027?>   end
		 => nil 
		2.0.0-p451 :028 > method5(1,2,3)
		Total Semuanya 7
		 => [3, 4, 7]
		 
5. a.	Anggara:training_rails User$ rails g model user

		class CreateUsers < ActiveRecord::Migration
		  def change
		    create_table :users do |t|
		      t.string :first_name
		      t.string :last_name
		      t.string :email
		      t.string :user_name
		      t.string :password
		      t.string :bio_profile
		      t.timestamps
		    end
		  end
		end

		Anggara:training_rails User$ rake db:migrate
		
		
   b.	Anggara:training_rails User$ rails g model country
   
   		class CreateCountries < ActiveRecord::Migration
		  def change
		    create_table :countries do |t|
		      t.integer :code
		      t.string :name
		      t.timestamps
		    end
		  end
		end
		
		Anggara:training_rails User$ rake db:migrate
	
   c.	Anggara:training_rails User$ rails g model article
   
   		class CreateArticles < ActiveRecord::Migration
		  def change
		    create_table :articles do |t|
		      t.string :title
		      t.string :body
		      t.timestamps
		    end
		  end
		end
		
		Anggara:training_rails User$ rake db:migrate

   d.	Anggara:training_rails User$ rails g model comment
   		
		class CreateComments < ActiveRecord::Migration
		  def change
		    create_table :comments do |t|
		      t.string :body
		      t.timestamps
		    end
		  end
		end

		
		Anggara:training_rails User$ rake db:migrate


6. a.	Anggara:training_rails User$ rails g migration add_column_to_user
		
		class AddColumnToUser < ActiveRecord::Migration
		  def change
		    add_column :users, :date_of_birth, :string
		    add_column :users, :age, :integer
		    add_column :users, :address, :string
		  end
		end
		
		Anggara:training_rails User$ rake db:migrate
		
   b.	Anggara:training_rails User$ rails g migration remove_column_user
   		
		class RemoveColumnUser < ActiveRecord::Migration
		  def change
		    remove_column :users, :bio_profile
		  end
		end
		
   		Anggara:training_rails User$ rake db:migrate
		
   c.	Anggara:training_rails User$ rails g migration change_column_table_country_and_article
   		
		class ChangeColumnTableCountryAndArticle < ActiveRecord::Migration
		  def change
		    change_column :countries, :code, :string
		    change_column :articles, :body, :text
		  end
		end
		
   		Anggara:training_rails User$ rake db:migrate
		
   d.	Anggara:training_rails User$ rails g migration rename_column_user_and_comment
   		
		class RenameColumnUserAndComment < ActiveRecord::Migration
		  def change
		    rename_column :users, :user_name, :username
		    rename_column :comments, :body, :content
		  end
		end
		
   		Anggara:training_rails User$ rake db:migrate


7. Create 5 dummy data for each table


- Table Users
		
users = User.create ({ :first_name => "Anggara", :last_name => "Siswanajaya", :email => "anggara.siswanajaya@gmail.com", :username => "emplud", :password => "upilwangilavender", :date_of_birth => "31-08-1991", :age => "22", :address => "Komp. Permata Arcamanik F-9" })

users = User.create ({ :first_name => "Tarjo", :last_name => "Mangkuwono", :email => "sayaganteng@gmail.com", :username => "tarjoe", :password => "tarjoeganteng", :date_of_birth => "01-01-2001", :age => "13", :address => "Wonosobo" })

users = User.create ({ :first_name => "Jokowi", :last_name => "Jokowow", :email => "akurapopo@gmail.com", :username => "jokowow", :password => "ambune", :date_of_birth => "13-12-1974", :age => "43", :address => "Slipi, Jakarta Barat" })

users = User.create ({ :first_name => "Joko", :last_name => "Bodo", :email => "dukun_beranak@yahoo.com", :username => "akibodoemang", :password => "misteri", :date_of_birth => "08-08-1988", :age => "26", :address => "Balikpapan, Kalimantan" })

users = User.create ({ :first_name => "Juli", :last_name => "Estelle", :email => "juli_palu@yahoo.com", :username => "juli_tumbuk", :password => "maudipalu", :date_of_birth => "12-03-1989", :age => "25", :address => "Kampung Rambutan, Jakarta Timur" })


- Table Articles

articles = Article.create ({ :title => "Exercise 1", :body => "lorem ipsum lorem ipsum" })

articles = Article.create ({ :title => "Exercise 2", :body => "lorem ipsum lorem ipsum" })

articles = Article.create ({ :title => "Exercise 3", :body => "lorem ipsum lorem ipsum" })

articles = Article.create ({ :title => "Exercise 4", :body => "lorem ipsum lorem ipsum" })

articles = Article.create ({ :title => "Exercise 5", :body => "lorem ipsum lorem ipsum" })


- Tables Countries

countries = Country.create ({ :code => "JKT48", :name => "Jakarta" })

countries = Country.create ({ :code => "BDG48", :name => "Bandung" })

countries = Country.create ({ :code => "GRT48", :name => "Garut" })

countries = Country.create ({ :code => "BGR48", :name => "Bogor" })

countries = Country.create ({ :code => "PPA48", :name => "Papua" })


- Tables Comments

comments = Comment.create ({ :content => "Aku Rapopo 1" })

comments = Comment.create ({ :content => "Aku Rapopo 2" })

comments = Comment.create ({ :content => "Aku Rapopo 3" })

comments = Comment.create ({ :content => "Aku Rapopo 4" })

comments = Comment.create ({ :content => "Aku Rapopo 5" })

8.

CREATE

2.0.0-p451 :002 > comments = Comment.new
 => #<Comment id: nil, content: nil, created_at: nil, updated_at: nil> 
2.0.0-p451 :003 > comments.content = "Okelah kalau begitu"
 => "Okelah kalau begitu" 
2.0.0-p451 :004 > comments.save
   (0.3ms)  BEGIN
  SQL (0.4ms)  INSERT INTO `comments` (`content`, `created_at`, `updated_at`) VALUES ('Okelah kalau begitu', '2014-05-09 03:10:53', '2014-05-09 03:10:53')
   (103.9ms)  COMMIT
 => true 

READ

2.0.0-p451 :037 > @user = User.find(1)
  User Load (32.4ms)  SELECT  `users`.* FROM `users`  WHERE `users`.`id` = 1 LIMIT 1
 => #<User id: 1, first_name: "Anggara", last_name: "Siswanajaya", email: "anggara.siswanajaya@gmail.com", username: "emplud", password: "upilwangilavender", created_at: "2014-05-09 02:35:41", updated_at: "2014-05-09 02:35:41", date_of_birth: "31-08-1991", age: 22, address: "Komp. Permata Arcamanik F-9"> 
 
2.0.0-p451 :038 > @customer = User.find(2)
  User Load (0.5ms)  SELECT  `users`.* FROM `users`  WHERE `users`.`id` = 2 LIMIT 1
 => #<User id: 2, first_name: "Tarjo", last_name: "Mangkuwono", email: "sayaganteng@gmail.com", username: "tarjoe", password: "tarjoeganteng", created_at: "2014-05-09 02:35:41", updated_at: "2014-05-09 02:35:41", date_of_birth: "01-01-2001", age: 13, address: "Wonosobo"> 
 
2.0.0-p451 :039 > Article.find(3)
  Article Load (0.7ms)  SELECT  `articles`.* FROM `articles`  WHERE `articles`.`id` = 3 LIMIT 1
 => #<Article id: 3, title: "Exercise 3", body: "lorem ipsum lorem ipsum", created_at: "2014-05-09 02:36:04", updated_at: "2014-05-09 02:36:04"> 
 
2.0.0-p451 :007 >   @users = User.find_by_first_name("Anggara")
  User Load (0.5ms)  SELECT  `users`.* FROM `users`  WHERE `users`.`first_name` = 'Anggara' LIMIT 1
 => #<User id: 1, first_name: "Anggara", last_name: "Siswanajaya", email: "anggara.siswanajaya@gmail.com", username: "emplud", password: "upilwangilavender", created_at: "2014-05-09 02:35:41", updated_at: "2014-05-09 02:35:41", date_of_birth: "31-08-1991", age: 22, address: "Komp. Permata Arcamanik F-9">

2.0.0-p451 :016 >   @users = User.find_by_first_name_and_last_name("Anggara", "Siswanajaya")
  User Load (0.6ms)  SELECT  `users`.* FROM `users`  WHERE `users`.`first_name` = 'Anggara' AND `users`.`last_name` = 'Siswanajaya' LIMIT 1
 => #<User id: 1, first_name: "Anggara", last_name: "Siswanajaya", email: "anggara.siswanajaya@gmail.com", username: "emplud", password: "upilwangilavender", created_at: "2014-05-09 02:35:41", updated_at: "2014-05-09 02:35:41", date_of_birth: "31-08-1991", age: 22, address: "Komp. Permata Arcamanik F-9"> 

2.0.0-p451 :019 >   users = User.find_by_sql("select first_name, username from users" )
  User Load (0.4ms)  select first_name, username from users
 => [#<User id: nil, first_name: "Anggara", username: "emplud">, #<User id: nil, first_name: "Tarjo", username: "tarjoe">, #<User id: nil, first_name: "Jokowi", username: "jokowow">, #<User id: nil, first_name: "Joko", username: "akibodoemang">, #<User id: nil, first_name: "Juli", username: "juli_tumbuk">] 
 
2.0.0-p451 :019 >   users = User.find_by_sql("select first_name, username from users" )
  User Load (0.4ms)  select first_name, username from users
 => [#<User id: nil, first_name: "Anggara", username: "emplud">, #<User id: nil, first_name: "Tarjo", username: "tarjoe">, #<User id: nil, first_name: "Jokowi", username: "jokowow">, #<User id: nil, first_name: "Joko", username: "akibodoemang">, #<User id: nil, first_name: "Juli", username: "juli_tumbuk">]
 
UPDATE
  
2.0.0-p451 :020 > user = User.update(2, :last_name => "Gorila")
  User Load (0.5ms)  SELECT  `users`.* FROM `users`  WHERE `users`.`id` = 2 LIMIT 1
   (0.2ms)  BEGIN
  SQL (53.2ms)  UPDATE `users` SET `last_name` = 'Gorila', `updated_at` = '2014-05-09 03:26:05' WHERE `users`.`id` = 2
   (0.9ms)  COMMIT
 => #<User id: 2, first_name: "Tarjo", last_name: "Gorila", email: "sayaganteng@gmail.com", username: "tarjoe", password: "tarjoeganteng", created_at: "2014-05-09 02:35:41", updated_at: "2014-05-09 03:26:05", date_of_birth: "01-01-2001", age: 13, address: "Wonosobo"> 

DELETE
 
2.0.0-p451 :021 > comments = Comment.delete(6)
  SQL (40.3ms)  DELETE FROM `comments` WHERE `comments`.`id` = 6
 => 1 
 
 
9. CREATE TABLE AND MIGRATION

CREATE TABLE

rails g model product
class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :price
      t.string :stock
      t.text :description 
      t.timestamps
    end
  end
end

rails g model product
class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.timestamps
    end
  end
end

rake db:migrate

CREATE MIGRATE

rails g migration add_change_rename_user_and_article
class AddChangeRenameUserAndArticle < ActiveRecord::Migration
  def change
    add_column :articles, :rating, :integer
    change_column :users, :address, :text
    rename_column :articles, :body, :description
  end
end

rake db:migrate


10. Relation Database Structure

1. User can create many products
User 	- has_many :products
Product - belongs_to :user
2.0.0-p451 :035 > @user.products.create ({ :name => "Unkl347", :price =>"100000" , :stock =>"10" , :description => "baju dari daun pisang"})
   (0.2ms)  BEGIN
  SQL (0.4ms)  INSERT INTO `products` (`created_at`, `description`, `name`, `price`, `stock`, `updated_at`, `user_id`) VALUES ('2014-05-09 07:08:37', 'baju dari daun pisang', 'Unkl347', '100000', '10', '2014-05-09 07:08:37', 1)
   (87.5ms)  COMMIT
 => #<Product id: 2, name: "Unkl347", price: "100000", stock: "10", description: "baju dari daun pisang", created_at: "2014-05-09 07:08:37", updated_at: "2014-05-09 07:08:37", user_id: 1> 
 
2. User can also writes many article
User	- has_many :articles
Article	- belongs_to :user
2.0.0-p451 :063 >   @user.articles.create({ :title => "Test_relation", :description => "lorem ipsum lorem ipsum", :rating =>"5" })
   (0.2ms)  BEGIN
  SQL (0.3ms)  INSERT INTO `articles` (`created_at`, `description`, `rating`, `title`, `updated_at`, `user_id`) VALUES ('2014-05-09 07:33:33', 'lorem ipsum lorem ipsum', 5, 'Test_relation', '2014-05-09 07:33:33', 1)
   (28.8ms)  COMMIT
 => #<Article id: 6, title: "Test_relation", description: "lorem ipsum lorem ipsum", created_at: "2014-05-09 07:33:33", updated_at: "2014-05-09 07:33:33", rating: 5, user_id: 1> 
 
3. User just have 1 nationality
Comment	- has_many :users
User	- belongs_to :country
2.0.0-p451 :090 > @country = Country.find_by_id(1)
  Country Load (0.5ms)  SELECT  `countries`.* FROM `countries`  WHERE `countries`.`id` = 1 LIMIT 1
 => #<Country id: 1, code: "JKT48", name: "Jakarta", created_at: "2014-05-09 02:37:21", updated_at: "2014-05-09 07:39:49"> 
2.0.0-p451 :091 > @country.users
  User Load (0.5ms)  SELECT `users`.* FROM `users`  WHERE `users`.`country_id` = 1
 => #<ActiveRecord::Associations::CollectionProxy [#<User id: 1, first_name: "Anggara", last_name: "Siswanajaya", email: "anggara.siswanajaya@gmail.com", username: "emplud", password: "upilwangilavender", created_at: "2014-05-09 02:35:41", updated_at: "2014-05-09 08:00:20", date_of_birth: "31-08-1991", age: 22, address: "Komp. Permata Arcamanik F-9">, #<User id: 2, first_name: "Tarjo", last_name: "Gorila", email: "sayaganteng@gmail.com", username: "tarjoe", password: "tarjoeganteng", created_at: "2014-05-09 02:35:41", updated_at: "2014-05-09 03:26:05", date_of_birth: "01-01-2001", age: 13, address: "Wonosobo">]>
 
4. User can create category for products.
User		- has_many :categories
Category	- belongs_to :user
2.0.0-p451 :100 > @user = User.find_by_id(1)
  User Load (0.4ms)  SELECT  `users`.* FROM `users`  WHERE `users`.`id` = 1 LIMIT 1
 => #<User id: 1, first_name: "Anggara", last_name: "Siswanajaya", email: "anggara.siswanajaya@gmail.com", username: "emplud", password: "upilwangilavender", created_at: "2014-05-09 02:35:41", updated_at: "2014-05-09 08:00:20", date_of_birth: "31-08-1991", age: 22, address: "Komp. Permata Arcamanik F-9", country_id: 1> 
2.0.0-p451 :101 > @user.categories.create ({ :name =>"Sepatu"})
   (0.1ms)  BEGIN
  SQL (0.5ms)  INSERT INTO `categories` (`created_at`, `name`, `updated_at`, `user_id`) VALUES ('2014-05-09 09:07:57', 'Sepatu', '2014-05-09 09:07:57', 1)
   (14.3ms)  COMMIT
 => #<Category id: 1, name: "Sepatu", created_at: "2014-05-09 09:07:57", updated_at: "2014-05-09 09:07:57", user_id: 1> 
 
5. User can write many comments
User		- has_many :categories
Category	- belongs_to :user
2.0.0-p451 :106 > @user = User.find_by_id(1)
  User Load (0.3ms)  SELECT  `users`.* FROM `users`  WHERE `users`.`id` = 1 LIMIT 1
 => #<User id: 1, first_name: "Anggara", last_name: "Siswanajaya", email: "anggara.siswanajaya@gmail.com", username: "emplud", password: "upilwangilavender", created_at: "2014-05-09 02:35:41", updated_at: "2014-05-09 08:00:20", date_of_birth: "31-08-1991", age: 22, address: "Komp. Permata Arcamanik F-9", country_id: 1> 
2.0.0-p451 :107 > @user.comments.create ({ :content =>"RELATION_TEST"})
   (0.2ms)  BEGIN
  SQL (0.2ms)  INSERT INTO `comments` (`content`, `created_at`, `updated_at`, `user_id`) VALUES ('RELATION_TEST', '2014-05-09 09:16:05', '2014-05-09 09:16:05', 1)
   (0.6ms)  COMMIT
 => #<Comment id: 7, content: "RELATION_TEST", created_at: "2014-05-09 09:16:05", updated_at: "2014-05-09 09:16:05", user_id: 1>
  
6. Articles can show comments
Article	- has_many :comments
Comment	- belongs_to :article
2.0.0-p451 :109 > @article = Article.find_by_id(1)
  Article Load (0.3ms)  SELECT  `articles`.* FROM `articles`  WHERE `articles`.`id` = 1 LIMIT 1
 => #<Article id: 1, title: "Exercise 1", description: "lorem ipsum lorem ipsum", created_at: "2014-05-09 02:36:04", updated_at: "2014-05-09 02:36:04", rating: nil, user_id: nil> 
2.0.0-p451 :110 > @article.comments
  Comment Load (0.3ms)  SELECT `comments`.* FROM `comments`  WHERE `comments`.`article_id` = 1
 => #<ActiveRecord::Associations::CollectionProxy [#<Comment id: 5, content: "Aku Rapopo 5", created_at: "2014-05-09 02:37:35", updated_at: "2014-05-09 02:37:35", user_id: 2, article_id: 1>, #<Comment id: 7, content: "RELATION_TEST", created_at: "2014-05-09 09:16:05", updated_at: "2014-05-09 09:16:05", user_id: 1, article_id: 1>]> 
 
7. All products can be categorized to all categories and alsoo all categories can have many products
Category			- has_many :products, :through => :categories_products
  			 	 	  has_many :categories_products
Product				- has_many :categories, :through => :categories_products
  			 	 	  has_many :categories_products
CategoriesProduct	- belongs_to :category
 	 				  belongs_to :product

2.0.0-p451 :031 > @category = Category.find_by_id(2)
  Category Load (0.4ms)  SELECT  `categories`.* FROM `categories`  WHERE `categories`.`id` = 2 LIMIT 1
 => #<Category id: 2, name: "Men", created_at: "2014-05-12 02:21:20", updated_at: "2014-05-12 02:21:20", user_id: 1> 
2.0.0-p451 :032 > @category.products
  Product Load (0.5ms)  SELECT `products`.* FROM `products` INNER JOIN `categories_products` ON `products`.`id` = `categories_products`.`product_id` WHERE `categories_products`.`category_id` = 2
 => #<ActiveRecord::Associations::CollectionProxy [#<Product id: 1, name: "Adidas", price: "400000", stock: "5", description: "material sepatu ini terbuat dari kulit nyamuk", created_at: "2014-05-09 06:32:17", updated_at: "2014-05-09 07:02:13", user_id: 1>, #<Product id: 2, name: "Unkl347", price: "100000", stock: "10", description: "baju dari daun pisang", created_at: "2014-05-09 07:08:37", updated_at: "2014-05-09 07:08:37", user_id: 1>]> 
2.0.0-p451 :033 > @product = Product.find_by_id(1)
  Product Load (0.5ms)  SELECT  `products`.* FROM `products`  WHERE `products`.`id` = 1 LIMIT 1
 => #<Product id: 1, name: "Adidas", price: "400000", stock: "5", description: "material sepatu ini terbuat dari kulit nyamuk", created_at: "2014-05-09 06:32:17", updated_at: "2014-05-09 07:02:13", user_id: 1> 
2.0.0-p451 :034 > @product.categories
  Category Load (0.5ms)  SELECT `categories`.* FROM `categories` INNER JOIN `categories_products` ON `categories`.`id` = `categories_products`.`category_id` WHERE `categories_products`.`product_id` = 1
 => #<ActiveRecord::Associations::CollectionProxy [#<Category id: 1, name: "Sepatu", created_at: "2014-05-09 09:07:57", updated_at: "2014-05-09 09:07:57", user_id: 1>, #<Category id: 2, name: "Men", created_at: "2014-05-12 02:21:20", updated_at: "2014-05-12 02:21:20", user_id: 1>]>
 
8. If article deleted the all comments is deleted too
Article	- has_many :comments, :dependent => :destroy
Comment	- belongs_to :article

2.0.0-p451 :035 > @article = Article.find_by_id(1)
  Article Load (0.3ms)  SELECT  `articles`.* FROM `articles`  WHERE `articles`.`id` = 1 LIMIT 1
 => #<Article id: 1, title: "Exercise 1", description: "lorem ipsum lorem ipsum", created_at: "2014-05-09 02:36:04", updated_at: "2014-05-09 02:36:04", rating: nil, user_id: nil> 
2.0.0-p451 :036 > @article.destroy
   (0.3ms)  BEGIN
  Comment Load (0.3ms)  SELECT `comments`.* FROM `comments`  WHERE `comments`.`article_id` = 1
  SQL (0.5ms)  DELETE FROM `comments` WHERE `comments`.`id` = 5
  SQL (0.2ms)  DELETE FROM `comments` WHERE `comments`.`id` = 7
  SQL (0.2ms)  DELETE FROM `articles` WHERE `articles`.`id` = 1
   (10.7ms)  COMMIT
 => #<Article id: 1, title: "Exercise 1", description: "lorem ipsum lorem ipsum", created_at: "2014-05-09 02:36:04", updated_at: "2014-05-09 02:36:04", rating: nil, user_id: nil> 

11. Create Scope for these conditions
 
1. I want to show all users that country from indonesia

User	- scope :indonesia, -> {where( "country_id = '1'")}
2.0.0-p451 :057 > User.indonesia
  User Load (0.4ms)  SELECT `users`.* FROM `users`  WHERE (country_id = '1')
 => #<ActiveRecord::Relation [#<User id: 1, first_name: "Anggara", last_name: "Siswanajaya", email: "anggara.siswanajaya@gmail.com", username: "emplud", password: "upilwangilavender", created_at: "2014-05-09 02:35:41", updated_at: "2014-05-09 08:00:20", date_of_birth: "31-08-1991", age: 22, address: "Komp. Permata Arcamanik F-9", country_id: 1>, #<User id: 2, first_name: "Tarjo", last_name: "Gorila", email: "sayaganteng@gmail.com", username: "tarjoe", password: "tarjoeganteng", created_at: "2014-05-09 02:35:41", updated_at: "2014-05-09 03:26:05", date_of_birth: "01-01-2001", age: 13, address: "Wonosobo", country_id: 1>]> 

2. I can search for products based on stock.

Product	- scope :stock, -> (stock) {where stock: stock}
2.0.0-p451 :078 > Product.stock(5)
  Product Load (0.3ms)  SELECT `products`.* FROM `products`  WHERE `products`.`stock` = 5
 => #<ActiveRecord::Relation [#<Product id: 1, name: "Adidas", price: "400000", stock: "5", description: "material sepatu ini terbuat dari kulit nyamuk", created_at: "2014-05-09 06:32:17", updated_at: "2014-05-09 07:02:13", user_id: 1>]>

3. I can search an articles based on a certain rating

Article	- scope :rating, -> (rating) {where rating: rating}
2.0.0-p451 :080 > Article.rating(5)
  Article Load (0.4ms)  SELECT `articles`.* FROM `articles`  WHERE `articles`.`rating` = 5
 => #<ActiveRecord::Relation [#<Article id: 6, title: "Test_relation", description: "lorem ipsum lorem ipsum", created_at: "2014-05-09 07:33:33", updated_at: "2014-05-09 07:33:33", rating: 5, user_id: 1>]> 

4. I want to show all category that containing the word "books"

Category	- scope :search, -> (search) {where name: search}
2.0.0-p451 :094 > Category.search("books")
  Category Load (0.3ms)  SELECT `categories`.* FROM `categories`  WHERE `categories`.`name` = 'books'
 => #<ActiveRecord::Relation [#<Category id: 3, name: "books", created_at: "2014-05-12 03:55:20", updated_at: "2014-05-12 03:55:20", user_id: nil>]> 

5. I want to show all products that price is smaller than 1000

2.0.0-p451 :076 > Product.under1k

Product	- scope :under1k, -> {where("price < '1000'")}
  Product Load (0.4ms)  SELECT `products`.* FROM `products`  WHERE (price < '1000')
 => #<ActiveRecord::Relation []> 
(Di dalam table products ga ada price dibawah 1000 jadi ga muncul)


12. Create an instance method and Class method for these conditions

1. I can get full address (address + country) from a user (Instance method)

  def show_address_country
    country = Country.find(self.country_id)
    "#{self.address} #{country.name}"
  end
  
2.0.0-p451 :123 > user = User.find_by_id("1")
  User Load (0.2ms)  SELECT  `users`.* FROM `users`  WHERE `users`.`id` = 1 LIMIT 1
 => #<User id: 1, first_name: "Anggara", last_name: "Siswanajaya", email: "anggara.siswanajaya@gmail.com", username: "emplud", password: "upilwangilavender", created_at: "2014-05-09 02:35:41", updated_at: "2014-05-09 08:00:20", date_of_birth: "31-08-1991", age: 22, address: "Komp. Permata Arcamanik F-9", country_id: 1> 
2.0.0-p451 :124 > user.show_address_country
  Country Load (0.2ms)  SELECT  `countries`.* FROM `countries`  WHERE `countries`.`id` = 1 LIMIT 1
 => "Komp. Permata Arcamanik F-9 Indonesia" 
 
2. I can find all articles that have a content more than 100 character (Class Method)

  def self.show_length
    where("length(description) > 100")
  end

2.0.0-p451 :175 > Article.show_length
  Article Load (0.4ms)  SELECT `articles`.* FROM `articles`  WHERE (length(description) > 100)
 => #<ActiveRecord::Relation []> 
(di database ga ada description lebih dari 100, jadi ga muncul)

3. I can find all users that the age is greater than 18. (Class method)

  def self.show_age
    where("age > '18'")
  end

2.0.0-p451 :183 > User.show_age
  User Load (0.4ms)  SELECT `users`.* FROM `users`  WHERE (age > '18')
 => #<ActiveRecord::Relation [#<User id: 1, first_name: "Anggara", last_name: "Siswanajaya", email: "anggara.siswanajaya@gmail.com", username: "emplud", password: "upilwangilavender", created_at: "2014-05-09 02:35:41", updated_at: "2014-05-09 08:00:20", date_of_birth: "31-08-1991", age: 22, address: "Komp. Permata Arcamanik F-9", country_id: 1>, #<User id: 3, first_name: "Jokowi", last_name: "Jokowow", email: "akurapopo@gmail.com", username: "jokowow", password: "ambune", created_at: "2014-05-09 02:35:41", updated_at: "2014-05-09 02:35:41", date_of_birth: "13-12-1974", age: 43, address: "Slipi, Jakarta Barat", country_id: nil>, #<User id: 4, first_name: "Joko", last_name: "Bodo", email: "dukun_beranak@yahoo.com", username: "akibodoemang", password: "misteri", created_at: "2014-05-09 02:35:41", updated_at: "2014-05-09 02:35:41", date_of_birth: "08-08-1988", age: 26, address: "Balikpapan, Kalimantan", country_id: nil>, #<User id: 5, first_name: "Juli", last_name: "Estelle", email: "juli_palu@yahoo.com", username: "juli_tumbuk", password: "maudipalu", created_at: "2014-05-09 02:35:41", updated_at: "2014-05-09 02:35:41", date_of_birth: "12-03-1989", age: 25, address: "Kampung Rambutan, Jakarta Timur", country_id: nil>, #<User id: 6, first_name: "Anggara_TEST", last_name: "Siswanajaya", email: "anggara.siswanajaya@gmail.com", username: "emplud", password: "upilwangilavender", created_at: "2014-05-09 03:32:46", updated_at: "2014-05-09 03:32:46", date_of_birth: "31-08-1991", age: 22, address: "Komp. Permata Arcamanik F-9", country_id: nil>]> 

13. Create an Override relation

1. I can search all articles from a user that the title of the article containing the words "my country"

  has_many :search_title,
             :class_name => "Article" ,
               :foreign_key => "user_id",
               :conditions => "title likes '%my country%'"

2.0.0-p451 :035 > user= User.find_by_id(1)
  User Load (0.3ms)  SELECT `users`.* FROM `users` WHERE `users`.`id` = 1 LIMIT 1
 => #<User id: 1, first_name: "Anggara", last_name: "Siswanajaya", email: "anggara.siswanajaya@gmail.com", username: "emplud", password: "upilwangilavender", created_at: "2014-05-09 02:35:41", updated_at: "2014-05-09 08:00:20", date_of_birth: "31-08-1991", age: 22, address: "Komp. Permata Arcamanik F-9", country_id: 1> 
2.0.0-p451 :036 > user.search_title
  Article Load (0.2ms)  SELECT `articles`.* FROM `articles` WHERE `articles`.`user_id` = 1 AND (title like '%my country%')
 => [] 
2. I can search all product from a category that the name of the product containing the words "shoes"
  has_and_belongs_to_many :search_products,
                           :class_name => "Product" ,
                           :foreign_key => "category_id",
                           :conditions => "name like '%shoes%'"

2.0.0-p451 :043 > category = Category.find_by_id(1)
  Category Load (0.2ms)  SELECT `categories`.* FROM `categories` WHERE `categories`.`id` = 1 LIMIT 1
 => #<Category id: 1, name: "Sepatu", created_at: "2014-05-09 09:07:57", updated_at: "2014-05-09 09:07:57", user_id: 1> 
2.0.0-p451 :044 > category.search_products
  Product Load (33.1ms)  SELECT `products`.* FROM `products` INNER JOIN `categories_products` ON `products`.`id` = `categories_products`.`product_id` WHERE `categories_products`.`category_id` = 1 AND (name like '%shoes%')
 => [] 

3. I can search all user from a countries that the name of the country is "indonesia"
  has_many :search_user_indo,
             :class_name => "User" ,
               :foreign_key => "country_id",
               :conditions => "country_id = '1'"

2.0.0-p451 :037 > country = Country.find_by_id(1)
  Country Load (0.2ms)  SELECT `countries`.* FROM `countries` WHERE `countries`.`id` = 1 LIMIT 1
 => #<Country id: 1, code: "INA", name: "Indonesia", created_at: "2014-05-09 02:37:21", updated_at: "2014-05-09 07:39:49", user_id: nil> 
2.0.0-p451 :038 > country.search_user_indo
  User Load (0.3ms)  SELECT `users`.* FROM `users` WHERE `users`.`country_id` = 1 AND (country_id = '1')
 => [#<User id: 1, first_name: "Anggara", last_name: "Siswanajaya", email: "anggara.siswanajaya@gmail.com", username: "emplud", password: "upilwangilavender", created_at: "2014-05-09 02:35:41", updated_at: "2014-05-09 08:00:20", date_of_birth: "31-08-1991", age: 22, address: "Komp. Permata Arcamanik F-9", country_id: 1>, #<User id: 2, first_name: "Tarjo", last_name: "Gorila", email: "sayaganteng@gmail.com", username: "tarjoe", password: "tarjoeganteng", created_at: "2014-05-09 02:35:41", updated_at: "2014-05-09 03:26:05", date_of_birth: "01-01-2001", age: 13, address: "Wonosobo", country_id: 1>] 
 
 
14. I have a validation for my data
1. First name, last name and email of user cannot nill and uniq. First and last name cannot contain a number and cannot have more than 25 characters.

-Class User
  validates :first_name,  :presence => true, 
                          :uniqueness => true,
                          :length => {:minimum => 1, :maximum => 25},
                          :format => {:with => /[a-zA-Z\s]+$/}
  validates :last_name,   :presence => true, 
                          :uniqueness => true,
                          :length => {:minimum => 1, :maximum => 25},
                          :format => {:with => /[a-zA-Z\s]+$/}
  validates :email,       :presence => true, 
                          :uniqueness => true,
                          :length => {:minimum => 3, :maximum => 25},
                          :format => {:with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i}
						  
2. Country must be present and its value of code just can be filled by "id", "usa", and "chn"

-Class Country

  validate :valid_code

  def valid_code
    self.errors[:code] << "Only can filled by 'id, usa, and chn'" unless code == 'id' || code == 'usa' || code == 'chn'
  end

2.0.0-p451 :127 > Country.create ({:code => "chn", :name => "China" })
   (0.1ms)  BEGIN
  SQL (0.2ms)  INSERT INTO `countries` (`code`, `created_at`, `name`, `updated_at`, `user_id`) VALUES ('chn', '2014-05-14 04:32:30', 'China', '2014-05-14 04:32:30', NULL)
   (1.0ms)  COMMIT
 => #<Country id: 6, code: "chn", name: "China", created_at: "2014-05-14 04:32:30", updated_at: "2014-05-14 04:32:30", user_id: nil> 
2.0.0-p451 :128 > Country.create ({:code => "brz", :name => "Brazil" })
   (0.2ms)  BEGIN
   (0.1ms)  ROLLBACK
 => #<Country id: nil, code: "brz", name: "Brazil", created_at: nil, updated_at: nil, user_id: nil> 

3. Title on article cannot be blank and must be uniq. It's value just can't be filled by "nill""empty" and "blank"

  validates :title,   :presence => true, 
                      :uniqueness => true,
                      :format => {:with => /[a-zA-Z\s]+$/}
                      
  validate :valid_title

  def valid_title
    self.errors[:title] << "Can't filled by 'nil, empty, and blank'" unless code != 'nil' || code != 'empty' || code != 'blank'
  end

4. Price on product must be filled by numericality.

  validates :price,   :presence => true,
                      :numericality => true
					  
5. All field on all tables must be filled if there is not filled will display a message, ex : "you must filled username"

:message => "You must filled first name" #contoh


15. Test my validation, scope, and instance method

16. My Authentication user
-Page new.html and def create (Articles, users, product, category, and country)
-Page edit.html and def edit (Articles, users, product, category, and country)
-index page for show all (Articles, users, product, category, and country)
-Def destroy for deleting an (Articles, users, product, category, country, and comments)
-i want to show the error if there are invalid data when create articles, users, product category, country and comments.
-Setting root to list article



